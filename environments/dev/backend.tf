terraform {
  backend "s3" {
    bucket  = "assignment-2-terraform-state"
    key     = "dev/terraform.tfstate"
    region  = "ap-southeast-1"
    encrypt = true
    #dynamodb_table = "terraform-lock"
  }
}
