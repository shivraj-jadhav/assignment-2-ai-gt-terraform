module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 20.0"

  cluster_name    = var.eks_cluster_name
  cluster_version = var.eks_cluster_version

  cluster_endpoint_public_access = true
  create_cloudwatch_log_group    = false

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  enable_cluster_creator_admin_permissions = true
  enable_irsa                              = true

  cluster_addons = {
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
    aws-ebs-csi-driver = {
      most_recent = true
    }
  }

  eks_managed_node_groups = {
    primary = {
      desired_size = var.managed_node_group_primary_desired_size
      min_size     = var.managed_node_group_primary_min_size
      max_size     = var.managed_node_group_primary_max_size

      labels = {
        role = "primary"
      }

      instance_types = var.managed_node_group_primary_instance_types
      capacity_type  = var.managed_node_group_primary_capacity_type

      iam_role_additional_policies = {
        AmazonEBSCSIDriverPolicy = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
      }
    }
  }

  node_security_group_additional_rules = {
    ingress_allow_access_on_http_port = {
      type                          = "ingress"
      protocol                      = "tcp"
      from_port                     = 80
      to_port                       = 80
      source_cluster_security_group = false
      description                   = "Allow access from port 80 to the app pods from the alb"
    }
  }

  tags = {
    Environment = var.tags_environment
  }
}

module "eks-aws-auth" {
  source  = "terraform-aws-modules/eks/aws//modules/aws-auth"
  version = "~> 20.0"

  manage_aws_auth_configmap = true
  aws_auth_roles = [
    {
      rolearn  = module.eks_admins_iam_role.iam_role_arn
      username = module.eks_admins_iam_role.iam_role_name
      groups   = ["system:masters"]
    }
  ]

  aws_auth_users = [
    {
      userarn  = "arn:aws:iam::381492122990:user/terraform"
      username = "terraform"
      groups   = ["system:masters"]
    }
  ]

  aws_auth_accounts = [
    "381492122990"
  ]
}

data "aws_eks_cluster" "default" {
  name       = module.eks.cluster_name
  depends_on = [module.eks.cluster_name]
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.default.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.default.certificate_authority[0].data)
  # token                  = data.aws_eks_cluster_auth.default.token

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    args        = ["eks", "get-token", "--cluster-name", data.aws_eks_cluster.default.name]
    command     = "aws"
  }
}
