module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  # version = "3.14.3"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs             = slice(data.aws_availability_zones.available.names, 0, 3)
  private_subnets = var.private_subnets_cidr
  public_subnets  = var.public_subnets_cidr

  public_subnet_tags = {
    "kubernetes.io/role/elb" = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = "1"
  }

  create_igw             = true
  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false

  enable_dns_hostnames = true
  #   enable_dns_support   = true

  tags = {
    Environment = var.tags_environment
  }
}