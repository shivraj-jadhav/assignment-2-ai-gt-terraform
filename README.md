# Assignment 2 AI GT Terraform

# GovTech

## Overview

Deploy an Amazon EKS cluster within the AWS ap-southeast-1 region using Infrastructure as Code (IaC) with Terraform.
Use GitLab IaC pipeline to create AWS EKS clusters for environments: dev, staging, prod. Each environment will have a different AWS EKS cluster set up.
- **dev:** `dev-eks`
- **staging:** `staging-eks`
- **prod:** `prod-eks`

[AWS infrastructure architecture diagram for the dev environment](https://drive.google.com/file/d/1ZvQfUkce-IUP2FV6JefepdIwJXSOReKA/view?usp=sharing)

## Prerequisites

Ensure the following GitLab CI/CD environment variables are set before running the pipeline:

- `AWS_ACCESS_KEY_ID`
- `AWS_DEFAULT_REGION`
- `AWS_SECRET_ACCESS_KEY`

### Infrastructure Provisioning with Terraform

- My Terraform code will set up the following resources in the AWS cloud:
    1. VPC
    2. Public and private subnets
    3. Security groups for AWS EKS managed node group
    4. ALB ingress controller in EKS cluster
    5. Internet gateway
    6. NAT gateway
    7. EKS auto scaler in the Kubernetes cluster
    8. EKS cluster addons: EBS CSI Driver
    9. EKS cluster
    10. IAM policies, groups, roles

- Set up 2 public subnets for high availability (HA) for the public ALB and 2 private subnets in different AZs for the AWS EKS managed node group for HA.
- Set up a remote S3 backend for the Terraform state file.
- Enabled server-side encryption for the S3 bucket to protect the stored content.
- Configured Application load balancer ingress controller in the AWS EKS cluster using Helm provider to expose Kubernetes services to the internet using ingress.
- Created IAM roles for service accounts(IRSA) for the 'kube-system:aws-load-balancer-controller' service account to control the AWS ALB.
- Configured Cluster Autoscaler in the AWS EKS cluster using Helm provider to automatically scale the number of worker nodes in the EKS managed node group as per the load. The Cluster Autoscaler integrates with the Kubernetes API server and directly monitors the state of your pods.
- Created IRSA for the 'kube-system:cluster-autoscaler' service account to control the AWS ASG for autoscaling the EKS nodes.
- The AWS EKS managed node group has also been configured to scale the number of EC2 instances using an autoscaling group based on predefined metrics on the underlying EC2 instances.
- Using my Terraform code I have also modified the 'aws-auth' configmap to add my IAM user 'terraform' and IAM group 'dev-eks-admin-group' the EKS Kubernetes group 'system:masters' granting administrative privileges.


### Web Application Content

- The CDS visitor tracker app webpage will be accessible from the ingress external IP which is the AWS ALB public DNS endpoint on port 80. This will require you to run the CDS application CI/CD pipeline after creating the AWS infrastructure.

### GitLab CI/CD Pipeline

- Set up IaC pipeline using Terraform and GitLab CI to create infrastructure on AWS.
- The GitLab IaC pipeline is set up in such a way that it caters to deploying a multi-environment cluster set up project.
- I have used three separate directories for dev, staging, and prod environments. This will help in isolating the environments and each directory has its own 'backend.tf'. We use the same AWS S3 bucket but isolate the different environment state files by storing them in separate folders inside the S3 bucket.
- Each environment directory also has its own '.tfvars' file to supply its unique variable values while running the Terraform plan, apply commands.
- Created an IAM user for Terraform and passed the credentials to GitLab CI. Reduced permissions for the Terraform user to enhance security.
- Stages in GitLab CI - 'dev_terraform_plan', 'dev_terraform_apply', 'staging_terraform_plan', 'staging_terraform_apply', 'prod_terraform_plan', 'prod_terraform_apply' for dev and main branches.
- The dev branch pipeline will run the Terraform validate and Terraform plan for all three environments depending on which environment directory you have made changes to. eg.: '/environment/dev/' or '/environment/staging/' etc.
- Once the plan is according to your requirements, proceed with creating a PR from the dev branch to the main branch.
- After PR approval, the main branch GitLab CI/CD will trigger the Terraform apply, setting up the infrastructure in the AWS environment. The Terraform apply stage will also run for all three environments depending on which environment directory you have made changes to.

## Terraform CI/CD Pipeline Details

### Stages in GitLab CI

1. **dev_terraform_plan:**
   - Ensures dev Terraform configurations are syntactically correct.
   - Provides an overview of the changes to be applied for the dev infrastructure.
  
2. **dev_terraform_apply:**
   - Deploys dev infrastructure in the AWS environment.
  
1. **staging_terraform_plan:**
   - Ensures staging Terraform configurations are syntactically correct.
   - Provides an overview of the changes to be applied for the staging infrastructure.
  
2. **staging_terraform_apply:**
   - Deploys staging infrastructure in the AWS environment.

1. **prod_terraform_plan:**
   - Ensures prod Terraform configurations are syntactically correct.
   - Provides an overview of the changes to be applied for the prod infrastructure.
  
2. **prod_terraform_apply:**
   - Deploys prod infrastructure in the AWS environment.

## Observing the Web Application

- After the Terraform apply stage has run successfully we can see the application load balancer ingress controller deployed in the AWS EKS cluster 'kube-system' namespace.
- We can also see the cluster autoscaler deployed in the AWS EKS cluster 'kube-system' namespace.

## Further Improvements

1. **Implementing High Availability for S3 Bucket:**
   - Consider implementing cross-region replication for the S3 bucket to ensure high availability and disaster recovery.

2. **DNS Configuration:**
   - Use a DNS name and create subdomains using Route 53 if needed. Create A records for the public ALB.

3. **Enhancing Security:**
   - Configure SSL/TLS for the ALB to encrypt data in transit between clients and the load balancer.

4. **Monitoring:**
   - Implement application monitoring using Amazon CloudWatch to track web server performance metrics and user behavior.

5. **Security:**
   - Consider implementing a Web Application Firewall (WAF) for additional security against common web exploits.

5. **CloudFront CDN:**
   - Implement CloudFront CDN to enable caching and reduce latency for end-users to access the web app.

## Visuals

Please refer to the screenshots I have provided in the screenshots directory showing the various stages of this assignment.
